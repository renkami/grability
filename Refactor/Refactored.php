<?php
function post_confirm()
{
    $response = '';
    $id       = Input::get('service_id');
    $servicio = Service::find($id);
    if (is_null($servicio)) {
        $response = '3';
    } else {
        $driver_id = Input::get('driver_id');
        if ($servicio->status_id == '6') {
            $response = '2';
        } else if (is_null($servicio->driver_id) && $servicio->status_id == '1') {
            if ($servicio->user->uuid == '') {
                $response = '0';
            } else {
                processService($id, $driver_id);
                $response = '0';
            }
        } else {
            $response = '1';
        }
    }

    return Response::json(array('error' => $response));
}
function processService($id, $driver_id)
{
    $servicio = Service::update($id, [
        'driver_id' => $driver_id,
        'status_id' => '2'
    	]
    );
    Driver::update($driver_id, ["available" => '0']);
    $driverTmp = Driver::find($driver_id);
    Service::update($id, ['car_id' => $driverTmp->car_id]);
    //Notificar a usuario!!
    $pushMessage = '¡Tu servicio ha sido confirmado!';
    $push        = Push::make();
    if ($servicio->user->type == '1') {
        //iPhone
        $push->ios(
            $servicio->user->uuid,
            $pushMessage,
            1,
            'honk.wav',
            'Open',
            array('service_id' => $servicio->id)
        );
    } else {
        $push->android2(
            $servicio->user->uuid,
            $pushMessage,
            1,
            'default',
            'Open',
            array('service_id' => $servicio->id)
        );
    }
}
